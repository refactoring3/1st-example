import static org.junit.Assert.*;

import entities.Customer;
import entities.Movie;
import entities.Rental;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.*;
import org.junit.runner.RunWith;

@RunWith(JUnitParamsRunner.class)
public class CustomerTest {

    @Test
    public void should_return_no_debt_and_no_points_given_no_movies(){
        Customer customer = new Customer("Client1");

        String result = customer.statement();

        assertEquals("""
                Rental Record for Client1
                Amount owed is 0.0
                You earned 0 frequent renter points""", result);
    }

    @Test
    @Parameters({
            "0,1,2.0,1",
            "0,3,3.5,1",
            "1,1,3.0,1",
            "1,3,9.0,2",
            "2,1,1.5,1",
            "2,4,3.0,1"})
    public void should_return_same_Amount_when_1_movie_rented(
            int movieKind,
            int daysRented,
            double thisAmount,
            int renterPoints){
        Customer customer = new Customer("Client1");
        customer.addRental(new Rental(new Movie("testMovie",movieKind),daysRented));

        String result = customer.statement();

        assertEquals("Rental Record for Client1\n" +
                "\ttestMovie\t" + thisAmount + "\n" +
                "Amount owed is " + thisAmount + "\n" +
                "You earned " + renterPoints + " frequent renter points", result);
    }

    @Test
    @Parameters({
            "0,1,2.0,1,1,3.0,5.0,2",
            "0,3,3.5,1,3,9.0,12.5,3"})
    public void should_return_sum_of_Amounts_when_2_movie_rented(
            int movie1Kind,
            int movie1days,
            double movie1Amount,
            int movie2Kind,
            int movie2days,
            double movie2Amount,
            double totalAmount,
            int renterPoints){
        Customer customer = new Customer("Client1");
        customer.addRental(new Rental(new Movie("testMovie1",movie1Kind),movie1days));
        customer.addRental(new Rental(new Movie("testMovie2",movie2Kind),movie2days));

        String result = customer.statement();

        assertEquals("Rental Record for Client1\n" +
                "\ttestMovie1\t" + movie1Amount + "\n" +
                "\ttestMovie2\t" + movie2Amount + "\n" +
                "Amount owed is " + totalAmount + "\n" +
                "You earned " + renterPoints + " frequent renter points", result);
    }

    @Test
    public void should_return_no_debt_and_no_points_given_no_movies_in_html(){
        Customer customer = new Customer("Client1");

        String result = customer.htmlStatement();

        assertEquals("""
                <H1>Rentals for <EM>Client1</EM></H1><P>
                <P>You owe <EM>0.0</EM><P>
                On this rental you earned <EM>0</EM> frequent renter points<P>""", result);
    }

    @Test
    @Parameters({
            "0,1,2.0,1",
            "0,3,3.5,1",
            "1,1,3.0,1",
            "1,3,9.0,2",
            "2,1,1.5,1",
            "2,4,3.0,1"})
    public void should_return_same_Amount_when_1_movie_rented_in_html(
            int movieKind,
            int daysRented,
            double thisAmount,
            int renterPoints){
        Customer customer = new Customer("Client1");
        customer.addRental(new Rental(new Movie("testMovie",movieKind),daysRented));

        String result = customer.htmlStatement();

        assertEquals("<H1>Rentals for <EM>Client1</EM></H1><P>\n" +
                "testMovie: " + thisAmount + "<BR>\n" +
                "<P>You owe <EM>" + thisAmount + "</EM><P>\n" +
                "On this rental you earned <EM>" + renterPoints + "</EM> frequent renter points<P>", result);
    }

    @Test
    @Parameters({
            "0,1,2.0,1,1,3.0,5.0,2",
            "0,3,3.5,1,3,9.0,12.5,3"})
    public void should_return_sum_of_Amounts_when_2_movie_rented_in_html(
            int movie1Kind,
            int movie1days,
            double movie1Amount,
            int movie2Kind,
            int movie2days,
            double movie2Amount,
            double totalAmount,
            int renterPoints){
        Customer customer = new Customer("Client1");
        customer.addRental(new Rental(new Movie("testMovie1",movie1Kind),movie1days));
        customer.addRental(new Rental(new Movie("testMovie2",movie2Kind),movie2days));

        String result = customer.htmlStatement();

        assertEquals("<H1>Rentals for <EM>Client1</EM></H1><P>\n" +
                "testMovie1: " + movie1Amount + "<BR>\n" +
                "testMovie2: " + movie2Amount + "<BR>\n" +
                "<P>You owe <EM>" + totalAmount + "</EM><P>\n" +
                "On this rental you earned <EM>" + renterPoints + "</EM> frequent renter points<P>", result);
    }
}
